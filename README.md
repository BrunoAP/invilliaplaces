# Invillia Interview App - React Native 0.60.5 version!

## Running Locally

* First of all: `npm install` or `yarn install`

* To run on iOS, you need first to install Pods: `cd ios/ && pod install`

* To run on Android, you need first to jetify: `npx jetify`

### Running on Android
* `npx react-native run-android`

### Running on iOS
* `react-native run-ios`

## Generating Release APK
* In the root of the project, run `npx react-native run-android --variant=release`
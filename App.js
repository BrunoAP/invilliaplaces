console.disableYellowBox = true;
console.ignoredYellowBox = ['Warning:'];
import React from 'react';
import Navigator from './src/navigator/index';

const App = () => (
  <Navigator/>
);
export default App;

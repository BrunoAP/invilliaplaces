const baseUrl = 'https://maps.googleapis.com/maps/api/place';
const API_KEY = 'AIzaSyAEvV7ttZXW8gnTYdHa_bkneHIdGPWszts';

export const getPlaceDetails = (place_id) => {
  const language = 'pt-BR';
  const params = `&place_id=${place_id}&language=${language}`;
  return get(params, 'details');
}

export const getPlaces = (location) => {
  const radiusSearch = '800';
  const type = 'point_of_interest';
  const params = `&radius=${radiusSearch}&type=${type}&location=${location}`;
  return get(params, 'nearbysearch');
}

const get = (params, method) => {
  return fetcher(params, method);
}

const fetcher = (params, method) => {
  const url = `${baseUrl}/${method}/json?key=${API_KEY}${params}`;
  return fetch(url);
}
import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ActivityIndicator, StyleSheet, Linking } from 'react-native';
import Modal from 'react-native-modal';
import Icon from 'react-native-vector-icons/Ionicons';
import IconMD from 'react-native-vector-icons/MaterialCommunityIcons';

import { getPlaceDetails } from '../api/place';

const styles = StyleSheet.create({
  modal: { flex: 1 },
  view: {
    flex: 1,
    elevation: 4,
    borderRadius: 14,
    marginVertical: 20,
    marginHorizontal: 20,
    backgroundColor: '#fff',
  },
  closeView: {
    flex: 0.06,
    borderTopLeftRadius: 14,
    borderTopRightRadius: 14,
    alignItems: 'center',
    justifyContent: 'center',
  },
  contentView: {
    flex: 0.94,
    marginTop: 5,
    paddingHorizontal: 14,
  },
  textView: {
    marginTop: 8,
    flexDirection: 'row',
  },
  colIcon: { flex: 0.1, marginRight: 10 },
  colText: { flex: 0.9 },
  title: {
    fontSize: 26,
    fontWeight: 'bold',
    textAlign: 'center',
    marginBottom: 10,
  },
  address: {
    fontSize: 16,
  },
  phone: {
    fontSize: 16,
    color: '#1a0dab',
    textDecorationLine: 'underline',
  },
  website: {
    fontSize: 16,
    color: '#1a0dab',
    textDecorationLine: 'underline',
  },
  hour: {
    fontSize: 16,
    marginRight: 10,
  },
  detailHour: {
    flexDirection: 'row',
  },
  dayName: {
    flex: 1,
    marginLeft: 10,
    fontSize: 16,
  },
  dayHour: {
    flex: 1,
    fontSize: 16,
  },
});

export default class PlaceDetails extends Component {
  constructor(props) {
    super(props);
    this.state = {
      details: {},
      loading: true,
      showingHours: false,
    };
  }

  getPhoneLink = (phone) => {
    return `tel:${(phone || '').replace(/ /g, '')}`;
  }

  getOpenLabel = (opening_hours) => {
    const weekday = new Date().getDay();
    let openLabel = opening_hours && opening_hours.open_now === true ? 'Aberto' : 'Fechado';
    if(opening_hours && opening_hours.periods) {
      const period = opening_hours.periods.filter(p => {
        return p.close.day === weekday || p.open.day === weekday;
      }) || {};
      if(period && period.open) {
        const { open, close } = period;
        const openTime = `${open.time.slice(0, 2)}:${open.time.slice(2)}`;
        const closeTime = `${close.time.slice(0, 2)}:${close.time.slice(2)}`;
        openLabel = opening_hours.open_now === true ? `${openLabel} até às ${closeTime}` : `${openLabel} - Abre às ${openTime}`;
      }
    }

    return openLabel;
  }

  fetchPlaceDetails = () => {
    getPlaceDetails(this.props.placeId).then(data => data.json()).then(({ result }) => {
      if(result && result.id) {
        const { name, formatted_address, formatted_phone_number, international_phone_number, opening_hours, url, website, photos } = result;
        const details = { name, formatted_address, formatted_phone_number, international_phone_number, opening_hours, url, website, photos };
        this.setState({ details, loading: false });
      }
    }).catch(err => {
      console.log('Get details err: ', err);
    });
  }

  onClose = () => {
    this.setState({ details: {}, loading: true, showingHours: false });
    this.props.closeModal();
  }

  onSwipe = (info) => {
    if(info && info.swipingDirection === 'down') {
      return this.onClose();
    }
  }

  onClickLink = (url) => {
    Linking.canOpenURL(url).then(value => {
      if(value === true) {
        return Linking.openURL(url);
      }
    }).catch(err => {
      console.log('Error opening URL: ' , err);
    });
  }

  onToggleHours = () => {
    this.setState({ showingHours: !this.state.showingHours });
  }

  renderHoursList = (weekdays) => {
    return (weekdays.map((weekday, i) => {
      const currentDay = new Date().getDay() === 0 ? 7 : new Date().getDay();
      const dayParts = weekday.split(': ');
      const dayName = dayParts[0].replace('-feira', '');
      const dayHour = dayParts[1];
      const addTextStyle = i === (currentDay-1) ? { fontWeight: 'bold' } : {};
      return (
        <View key={i} style={styles.detailHour}>
          <View style={styles.colIcon}/>
          <Text style={[styles.dayName, addTextStyle]}>{dayName}</Text>
          <Text style={[styles.dayHour, addTextStyle]}>{dayHour}</Text>
        </View>
      );
    }));
  }

  render() {
    const { name, formatted_address, formatted_phone_number, international_phone_number, opening_hours, website } = this.state.details;
    const phoneLink = this.getPhoneLink(international_phone_number || '');
    const weekdays = (opening_hours || {}).weekday_text || [];
    const openLabel = this.getOpenLabel(opening_hours);

    return (
      <Modal
        animationIn={'slideInUp'}
        animationOut={'slideOutDown'}
        isVisible={this.props.show}
        swipeDirection={['down']}
        onSwipeComplete={this.onSwipe}
        onBackdropPress={this.onClose}
        onShow={this.fetchPlaceDetails}
      >
        <View style={styles.view}>
          <TouchableOpacity style={styles.closeView} onPress={this.onClose}>
            <Icon name={'ios-arrow-down'} color={'#666'} size={32}/>
          </TouchableOpacity>
          <View style={styles.contentView}>
            {this.state.loading && <ActivityIndicator size={'large'} />}
            <Text style={styles.title}>
              {name}
            </Text>
            {formatted_address &&
              <View style={styles.textView}>
                <View style={styles.colIcon}>
                  <IconMD name={'map-marker-outline'} size={23}/>
                </View>
                <View style={styles.colText}>
                  <Text style={styles.address}>
                    {formatted_address}
                  </Text>
                </View>
              </View>
            }
            {formatted_phone_number &&
              <View style={styles.textView}>
                <View style={styles.colIcon}>
                  <IconMD name={'phone'} size={23}/>
                </View>
                <TouchableOpacity style={styles.colText} onPress={() => this.onClickLink(phoneLink)}>
                  <Text style={styles.phone}>
                    {formatted_phone_number}
                  </Text>
                </TouchableOpacity>
              </View>
            }
            {website &&
              <View style={styles.textView}>
                <View style={styles.colIcon}>
                  <IconMD name={'web'} size={23}/>
                </View>
                <TouchableOpacity style={styles.colText} onPress={() => this.onClickLink(website)}>
                  <Text style={styles.website}>
                    {website}
                  </Text>
                </TouchableOpacity>
              </View>
            }
            {name && opening_hours &&
              <View style={styles.textView}>
                <View style={styles.colIcon}>
                  <IconMD name={'clock-outline'} size={23}/>
                </View>
                <TouchableOpacity style={[styles.colText, { flexDirection: 'row' }]} onPress={this.onToggleHours}>
                  <Text style={styles.hour}>
                    {openLabel}
                  </Text>
                  <IconMD
                    size={25}
                    // color={'#1a0dab'}
                    name={this.state.showingHours ? 'minus' : 'plus'}
                  />
                </TouchableOpacity>
              </View>
            }
            {(this.state.showingHours && weekdays.length > 0) && this.renderHoursList(weekdays)}
          </View>
        </View>
      </Modal>
    );
  }
}
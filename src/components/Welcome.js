import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ImageBackground, StyleSheet } from 'react-native';

const image = require('../assets/background.jpeg');

const styles = StyleSheet.create({
  container: { flex: 1 },
  image: {
    flex: 1,
    alignItems: 'center',
  },
  title: {
    flex: 0.4,
    justifyContent: 'flex-end'
  },
  titleText: {
    fontSize: 32,
    paddingHorizontal: 8,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#FAFAFA'
  },
  obs: {
    flex: 0.4,
    justifyContent: 'center'
  },
  obsText: {
    paddingHorizontal: 20,
    fontSize: 20,
    textAlign: 'center',
    color: '#FAFAFA'
  },
  button: {
    bottom: 10,
    position: 'absolute',
    backgroundColor: '#2185d0',
    width: '86%',
    elevation: 4,
    borderRadius: 6,
    paddingVertical: 10,
    alignItems: 'center',
  },
  textButton: {
    fontSize: 16,
    color: '#fff',
    textAlign: 'center',
    fontWeight: 'bold',
  },
});

export default class Welcome extends Component {
  goToMap = () => {
    this.props.navigation.push('Home');
  }

  render() {
    return (
      <View style={styles.container}>
        <ImageBackground source={image} style={styles.image}>
          <View style={styles.title}>
            <Text style={styles.titleText}>Bem vindo ao Places!</Text>
          </View>
          <View style={styles.obs}>
            <Text style={styles.obsText}>
              Com o Places você poderá ver vários locais próximos a você, suas notas, informações de contato e muito mais!
              {'\n\n'}Você só precisa nos dar permissão para acessar sua Localização!
            </Text>
          </View>
          <TouchableOpacity style={styles.button} onPress={this.goToMap}>
            <Text style={styles.textButton}>Começar!</Text>
          </TouchableOpacity>
        </ImageBackground>
      </View>
    );
  }
}
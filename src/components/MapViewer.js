import React, { Component } from 'react';
import { View, TouchableOpacity, Alert, StyleSheet, Platform, PermissionsAndroid } from 'react-native';

import _ from 'lodash';
import Geolocation from 'react-native-geolocation-service';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import RNGooglePlaces from 'react-native-google-places';
import Icon from 'react-native-vector-icons/MaterialIcons';

import { getPlaces } from '../api/place';

import PlaceDetailsModal from './PlaceDetails';
import PlacesCarousel from './PlacesCarousel';

const isiOS = Platform.OS === 'ios';

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  map: {
    flex: 1
  },
  centerView: {
    elevation: 3,
    width: 50,
    height: 50,
    borderRadius: 25,
    top: '7%',
    right: '6%',
    position: 'absolute',
    shadowColor: '#444',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  searchView: {
    elevation: 3,
    width: 50,
    height: 50,
    borderRadius: 25,
    top: '17%',
    right: '6%',
    position: 'absolute',
    shadowColor: '#444',
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default class MapViewer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      //Location
      latitude: 0,
      longitude: 0,
      latitudeDelta: 0.005,
      longitudeDelta: 0.005,

      //User Location
      userLat: 0,
      userLng: 0,

      //Places
      places: [],
      fetchedPlaces: false,

      //Modal
      showModal: false,
      placeId: null,
    };
  }

  componentDidMount() {
    if(isiOS) this.getUserGeolocation();
    else this.requestAndroidPermission();
  }

  requestAndroidPermission = async () => {
    const isGranted = await PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
    if(!isGranted) {
      const grantedPermission = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
      if(!grantedPermission) {
        return Alert.alert('Permissão de Localização', 'Para utilizar o aplicativo, você precisa permitir o acesso à localização do seu dispositivo!');
      }
    }

    return this.getUserGeolocation();
  }

  getUserGeolocation = () => {
    if(isiOS) Geolocation.requestAuthorization();

    Geolocation.setRNConfiguration({ authorizationLevel: 'whenInUse', skipPermissionRequests: false });
    Geolocation.getCurrentPosition(({ coords }) => {
      const { latitude, longitude } = coords;
      this.setState({ latitude, longitude, userLat: latitude, userLng: longitude });
      this.fetchPlaces(`${latitude},${longitude}`);
    }, (err) => {
      if(err.code === 1) {
        Alert.alert('Permissão de Localização', 'Para utilizar o aplicativo, você precisa permitir o acesso à localização do seu dispositivo!', [
          { text: 'Depois' },
          { text: 'Permitir', onPress: this.requestAndroidPermission },
        ], { cancelable: true });
      } else {
        Alert.alert('Erro de Localização', err.message);
      }
    }, {
      enableHighAccuracy: true,
      showLocationDialog: true,
      forceRequestLocation: true,
    });
  }

  fetchPlaces = (coords) => {
    if(!this.state.fetchedPlaces) {
      this.setState({ fetchedPlaces: true });
      getPlaces(coords).then(data => data.json()).then(json => {
        const { results } = json;
        if(results && results.length > 0) {
          const types = ['point_of_interest', 'establishment'];
          const places = results.filter(p => p.types.includes(types[0]) || p.types.includes(types[1])).map(({
            name, geometry, place_id, rating, types, photos, vicinity
          }) => ({
            place_id,
            title: name,
            rating,
            types,
            address: vicinity,
            photo: (photos || [])[0] || null,
            coords: { latitude: geometry.location.lat, longitude: geometry.location.lng },
          }));
          const orderedPlaces = this.orderPlacesByGeolocationDistance(places);
          this.setState({ places: orderedPlaces }, () => {
            setTimeout(() => this.goToCardCarousel(null, 0), 100);
          });
        }
      }).catch(err => {
        console.log('Get places error: ', err);
      });
    }
  }

  orderPlacesByGeolocationDistance = (places) => {
    const { userLat, userLng } = this.state;
    const placesWithDistance = places.map((place) => {
      const { latitude, longitude } = place.coords;
      const diffLatsPow2 = Math.pow(Math.abs(userLat-latitude), 2);
      const diffLongsPow2 = Math.pow(Math.abs(userLng-longitude), 2);
      const distance = Math.sqrt(diffLatsPow2+diffLongsPow2);
      return { ...place, distance };
    });

    return _.orderBy(placesWithDistance, 'distance', 'asc');
  }

  goToMarker = (coords, placeId) => {
    const index = _.findIndex(this.state.places, { place_id: placeId });
    const { latitude, longitude } = coords;
    this.setState({ latitude, longitude });
    if(this[`marker${placeId}`] && this[`marker${placeId}`].showCallout) {
      this[`marker${placeId}`].showCallout();
    }
  }

  goToCardCarousel = (placeId, idx = null) => {
    const index = idx ? idx : _.findIndex(this.state.places, { place_id: placeId });
    if(this.carousel && this.carousel.snapToItem) {
      this.carousel.snapToItem(index, true, false);
    }
  }

  openPlaceDetails = (placeId, coords) => {
    const { latitude, longitude } = coords;
    this.goToCardCarousel(placeId);
    this.setState({ latitude, longitude }, () => {
      setTimeout(() => this.setState({ showModal: true, placeId }), 100);
    });
  }

  closePlaceDetails = () => {
    this.setState({ showModal: false, placeId: null });
  }

  openSearchModal = () => {
    const { places } = this.state;
    const placesCopy = _.cloneDeep(places);
    RNGooglePlaces.openAutocompleteModal({ type: 'point_of_interest' }).then((place) => {
      const { name, location, placeID, rating, types, photos, address } = place;
      const newPlace = {
        place_id: placeID,
        title: name,
        rating,
        types,
        address,
        photo: (photos || [])[0] || null,
        coords: { latitude: location.latitude, longitude: location.longitude },
      };
      placesCopy.push(newPlace);
      const orderedPlaces = this.orderPlacesByGeolocationDistance(placesCopy);
      this.setState({ places: orderedPlaces }, () => {
        this.goToCardCarousel(newPlace.place_id);
        this.goToMarker(newPlace.coords, newPlace.place_id);
      });
    }).catch(error => {
      console.log(error.message);
    });
  }

  renderPlaceMarker = (item, key) => {
    const { title, coords, place_id } = item;
    return (
      <Marker
        ref={r => this[`marker${place_id}`] = r}
        key={key}
        title={title}
        coordinate={coords}
        onPress={() => this.openPlaceDetails(place_id, coords)}
        onCalloutPress={() => this.openPlaceDetails(place_id, coords)}
      />
    );
  }

  render() {
    const { places, latitude, longitude, latitudeDelta, longitudeDelta } = this.state;
    return (
      <View style={styles.container}>
        <MapView
          style={styles.map}
          showsUserLocation={true}
          provider={PROVIDER_GOOGLE}
          region={{ latitude, longitude, latitudeDelta, longitudeDelta }}
        >
          {places.map(this.renderPlaceMarker)}
        </MapView>
        <TouchableOpacity style={styles.centerView} onPress={this.getUserGeolocation}>
          <Icon name={'my-location'} size={26} color={'#777'}/>
        </TouchableOpacity>
        <TouchableOpacity style={styles.searchView} onPress={this.openSearchModal}>
          <Icon name={'search'} size={26} color={'#777'}/>
        </TouchableOpacity>
        <PlaceDetailsModal
          show={this.state.showModal}
          placeId={this.state.placeId}
          closeModal={this.closePlaceDetails}
        />
        <PlacesCarousel
          places={places}
          goToMarker={this.goToMarker}
          openPlaceDetails={this.openPlaceDetails}
          getCarouselRef={(ref) => this.carousel = ref}
        />
      </View>
    );
  }
}
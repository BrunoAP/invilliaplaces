import React, { Component } from 'react';
import { View, TouchableOpacity, Text, StyleSheet, Dimensions } from 'react-native';
import Carousel from 'react-native-snap-carousel';
import Icon from 'react-native-vector-icons/MaterialIcons';

const { width } = Dimensions.get('screen');

const styles = StyleSheet.create({
  carousel: {
    bottom: 0,
    position: 'absolute',
  },
  card: {
    height: 100,
    elevation: 4,
    borderRadius: 10,
    marginHorizontal: 20,
    marginBottom: 20,
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  title: {
    fontSize: 20,
    paddingHorizontal: 6,
    marginTop: 10,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  starsView: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  icon: { marginTop: 1 },
  stars: {
    fontSize: 24,
    marginLeft: 5,
    color: '#888',
    textAlignVertical: 'center',
  },
  noStars: {
    fontSize: 17,
    color: '#888',
  },
});

export default class PlacesCarousel extends Component {
  onSnapToItem = (index) => {
    const place = this.props.places[index];
    this.props.goToMarker(place.coords, place.place_id);
  }

  renderItem = ({item, index}) => {
    const { title, coords, place_id, rating } = item;
    return (
      <TouchableOpacity
        key={index}
        style={styles.card}
        onPress={() => this.props.openPlaceDetails(place_id, coords)}
      >
        <Text style={styles.title} numberOfLines={2} ellipsizeMode={'tail'}>{title}</Text>
        <View style={styles.starsView}>
          {rating && <Icon style={styles.icon} name={'star'} color={'#face57'} size={30}/>}
          {rating && <Text style={styles.stars}>{rating}</Text>}
          {!rating && <Text style={styles.noStars}>Sem avaliação</Text>}
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    const { places } = this.props;
    const itemWidth = width;
    const sliderWidth = width;
    return (
      <Carousel
        ref={c => this.props.getCarouselRef(c)}
        data={places}
        renderItem={this.renderItem}
        itemWidth={itemWidth}
        sliderWidth={sliderWidth}
        horizontal={true}
        scrollEnabled={true}
        snapOnAndroid={true}
        containerCustomStyle={styles.carousel}
        onSnapToItem={this.onSnapToItem}
        loop={true}
      />
    );
  }
}